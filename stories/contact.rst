.. title: contact
.. slug: contact
.. date: 2017-11-06 11:00:03 UTC+13:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. hidetitle: True


| **Peter Reutemann**
| *MSc CS (Freiburg, Germany)*
| Senior Research Programmer

| Department of Computer Science
| The University of Waikato
| Private Bag 3105
| Hamilton 3240
| New Zealand

| `FG.2.03 <https://www.waikato.ac.nz/contacts/map/?ERI>`__ (ERI building)
| **P** +64 (7) 858-5174
| **F** +64 (7) 858-5095
| fracpete at waikato dot ac dot nz

profiles
========

* `LinkedIn <https://www.linkedin.com/in/fracpete/>`__
* `Github <https://github.com/fracpete>`__
* `OpenHub <https://www.openhub.net/accounts/31405?ref=Tiny>`__
* `ORCID <http://orcid.org/0000-0002-1226-0948>`__
* `ResearchGate <https://www.researchgate.net/profile/Peter_Reutemann>`__
* `Academia.edu <https://waikato.academia.edu/fracpete>`__
