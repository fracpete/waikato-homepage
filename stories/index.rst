.. title: home
.. slug: index
.. date: 2017-11-06 11:00:04 UTC+13:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. hidetitle: True

.. image:: images/mandel_closeup.png
   :height: 100px
   :align: right
   :class: logo-image

.. line-block::

   Open-source advocate and programmer by heart, 
   specialized in workflow engines,
   machine learning and data mining.



professional
============

* Day-to-day 
  [`adams <https://adams.cms.waikato.ac.nz/>`__] 
  [`ml <http://en.wikipedia.org/wiki/Machine_learning>`__]
  [`mooc <https://weka.waikato.ac.nz/>`__]
* Reviewer 
  [`jmlr/mloss <http://jmlr.csail.mit.edu/mloss/>`__]
* Programme committee 
  [`mloss15 <http://mloss.org/workshop/icml15/>`__] 
  [`mloss13 <http://mloss.org/workshop/nips13/>`__] 
  [`mloss10 <http://mloss.org/workshop/icml10/>`__]
* Session chair 
  [`eResearch NZ 2014 <https://adams.cms.waikato.ac.nz/ernz-2014/>`__]
* Teaching 
  [`comp314 <http://papers.waikato.ac.nz/papers/COMP314>`__] (2015-2017)

personal
========

* Waikato Linux User Group (WLUG) 
  [`www <http://www.wlug.org.nz/>`__] 
  [`meetup <http://www.meetup.com/WaikatoLinuxUsersGroup/>`__] 
  [`mailing list <http://list.waikato.ac.nz/mailman/listinfo/wlug/>`__]
* Python 
  [`www <http://nzpug.org/>`__] 
  [`meetup <http://www.meetup.com/nzpug-hamilton/>`__]
  [`mailing list <https://groups.google.com/forum/#!forum/nzpug>`__]
* Open-source blog 
  [`www <http://open.fracpete.org/>`__]

