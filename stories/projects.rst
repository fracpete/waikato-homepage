.. title: projects
.. slug: projects
.. date: 2017-11-06 11:00:01 UTC+13:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. hidetitle: True


.. line-block::

   "Not craftsmen, my lord," he said. 
   "I have no use for people who have learned the limits of the possible."

     Leonard of Quirm
     *The last hero, Terry Pratchett*


active
======

* ADAMS (Advanced Data mining and Machine learning System) 
  [`www <https://adams.cms.waikato.ac.nz/>`__] 
  [`youtube <http://www.youtube.com/user/TheAdamsWorkflow>`__]
* python-weka-wrapper 
  [`pypi <https://pypi.python.org/pypi/python-weka-wrapper>`__] 
  [`github <https://github.com/fracpete/python-weka-wrapper>`__]
* python-weka-wrapper3
  [`pypi <https://pypi.python.org/pypi/python-weka-wrapper3>`__] 
  [`github <https://github.com/fracpete/python-weka-wrapper3>`__]
* MEKA: A Multi-label Extension to WEKA - contributor (experiments, GUI) 
  [`www <http://meka.sourceforge.net/>`__]
* Weka - maintainer (09/2005-06/2007) and contributor (since 2004) 
  [`www <http://www.cms.waikato.ac.nz/~ml/weka/>`__]
* MOA (Massive Online Analysis) - contributor 
  [`www <http://moa.cms.waikato.ac.nz/>`__]
* Semi-Supervised Learning and Collective Classification 
  [`www <https://github.com/fracpete/collective-classification-weka-package>`__]
* jfilechooser-bookmarks 
  [`www <https://github.com/fracpete/jfilechooser-bookmarks>`__]
* rsync4j
  [`www <https://github.com/fracpete/rsync4j>`__]
* sizeofag (SizeOf Agent) - mavenization 
  [`www <https://github.com/fracpete/sizeofag>`__]
* **many more on** `github <https://github.com/fracpete/>`__ (`curated list <https://github.com/fracpete/projects>`__)


discontinued
============

* Kepler and Ptolemy II (aka KeplerWeka) 
  [`www <https://sourceforge.net/projects/keplerweka/>`__]
* Weka Proper (Master's Thesis) 
  [`www <https://www.cs.waikato.ac.nz/ml/proper/>`__] 
  [`pdf </pubs/2004/thesis.pdf>`__] 
  [`ps </pubs/2004/thesis.ps.gz>`__]
* vfsjfilechooser2 (JFileChooser alternative) - mavenization/maintenance 
  [`www <https://github.com/fracpete/vfsjfilechooser2>`__]

